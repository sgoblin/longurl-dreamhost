#!/usr/bin/env python
from __future__ import with_statement
from __future__ import absolute_import
import hashlib
import random
import sys, os
from io import open
INTERP = os.path.join(os.environ['HOME'], 'longurl.sgoblin.com', 'venv', 'bin', 'python')
if sys.executable != INTERP:
  os.execl(INTERP, INTERP, *sys.argv)
  
import falcon
import hirlite



longurlhost = u"http://longurl.sgoblin.com/"
rlite = hirlite.Rlite(encoding=u'utf8',path=u'longurl.rld')

with open(u'genurl.html', u'r') as genurlFile:
  genurlHTML = genurlFile.read()

with open(u'genfriend.html', u'r') as genfriendFile:
  genfriendHTML = genfriendFile.read()

def insertURL(urlToRedir):
  randomGen = random.SystemRandom()
  urlToStore = hashlib.sha512((unicode(randomGen.random()) + urlToRedir + unicode(randomGen.random())).encode(u'utf-8')).hexdigest()
  global rlite
  rlite.command(u'set', urlToStore, urlToRedir)
  return urlToStore

class IndexPage(object):
  def on_get(self, req, resp):
    resp.status = falcon.HTTP_200
    resp.content_type = u"text/html"
    resp.body = (u'''<!DOCTYPE html><html><head><title>sgoblin's long url generator</title></head><body>Look for more documentation soon!</body></html>''')
    
class GenURL(object):
  def on_get(self, req, resp):
    resp.status = falcon.HTTP_400
    resp.content_type = u"text/html"
    resp.body = genurlHTML
    
  def on_post(self, req, resp):
    requestBody = req.stream.read().decode(u'utf-8')
    print req.content_type
    urlThatWasStored = insertURL(requestBody)
    resp.status = falcon.HTTP_200
    resp.content_type = u'text/plain'
    resp.body = longurlhost + u'redir?url=' + urlThatWasStored
    
class GenFriend(object):
  def on_get(self, req, resp):
    resp.status = falcon.HTTP_200
    resp.content_type = u"text/html"
    resp.body = genfriendHTML
    
  def on_post(self, req, resp):
    print req.get_param(u'url')
    urlThatWasStored = insertURL(req.get_param(u'url'))
    resp.status = falcon.HTTP_200
    resp.content_type = u"text/plain"
    resp.body = u'Your longurl is: ' + longurlhost + u'redir?url=' + urlThatWasStored + u"\nand was generated from: " + req.get_param(u'url')
    
class RedirURL(object):
  def on_get(self, req, resp):
    queryString = falcon.util.uri.parse_query_string(req.query_string)
    
    try:
      resp.status = falcon.HTTP_302
      print queryString[u'url']
      resp.location = rlite.command(u'get', queryString[u'url'])
    except:
      resp.status = falcon.HTTP_400
      resp.content_type = u"text/html"
      resp.body = (u'''<!DOCTYPE html><html><head><title>sgoblin's long url generator</title></head><body>There should be a query string on here or the query string is invalid.</body></html>''')
  def on_head(self, req, resp):
    queryString = falcon.util.uri.parse_query_string(req.query_string)
    try:
      resp.status = falcon.HTTP_307
      print queryString[u'url']
      resp.location = rlite.command(u'get', queryString[u'url'])
    except:
      resp.status = falcon.HTTP_400
      resp.content_type = u"text/html"
      
application = falcon.API()

index = IndexPage()
genurl = GenURL()
redirurl = RedirURL()
genfriend = GenFriend()

application.add_route(u'/', index)
application.add_route(u'/gen', genurl)
application.add_route(u'/redir', redirurl)
application.add_route(u'/genform', genfriend)
